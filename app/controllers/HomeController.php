<?php

namespace app\controllers;
use app\core\Helpers;
use app\models\Quiz;

class HomeController extends MainController
{

    public static function home()
    {
        $Quiz = new Quiz();
        $questions = $Quiz->getAllMultipleQuestionsWithAnswers();

        return Helpers::view('index', $questions);
    }

    public static function binaryQuestions()
    {
        $Quiz = new Quiz();
        $type = 'binary';
        $questions = $Quiz->getAllBinaryQuestionsWithAnswers($type);

        return Helpers::view('index', $questions, $type);
    }

    public static function multipleAnswers($request)
    {
        $Quiz = new Quiz();
        $multipleAnswers = $Quiz->getMultipleAnswers($request);

        return json_encode($multipleAnswers);
    }

    public static function binaryAnswers($request){

        $Quiz = new Quiz();
        $binaryAnswer = $Quiz->getBinaryAnswers($request);
        return json_encode($binaryAnswer);
    }
}
