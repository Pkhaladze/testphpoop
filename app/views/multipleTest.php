<?php
    foreach ($data as $key => $question) {
        $number = $key + 1;
        print <<<END
                <div class="card p-2 mb-1">
                    <h4 class="pb-1">$number. $question[question]</h4>
END;
        foreach ($question['answers'] as $answer) {
            print <<<END
                    <div class="form-check">
                        <input 
                            onclick="httpPostAsync('http://localhost:1234/multiple-answer', handleMultipleAnswer, $question[id], $answer[id])"
                            class="form-check-input" 
                            type="radio" name="question-id-$question[id]"
                            id="answer-id-$answer[id]">
                        <label class="form-check-label" for="answer-id-$answer[id]">$answer[answer]</label>
                    </div>
END;
        }
        echo '<div class="alert mb-0 mt-1" id="alert-id-'. $question['id'] .'"></div>';
        echo '</div>';
    }
?>
<script>
    function httpPostAsync(Url, callback, questionId, answerId)
    {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.open("POST", Url, true);
        xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                callback(xmlHttp.responseText);
            }
        }
        xmlHttp.send('questionId=' + questionId + '&answerId=' + answerId);
    }
    function handleMultipleAnswer($data)
    {
        const answer = JSON.parse($data);
        let alert = document.getElementById('alert-id-' + answer['question_id']);
        if (answer['correct']){
            alert.innerHTML = '<b>Correct! The right answer is</b> - "' + answer['content'] + '"';
            alert.classList.add('alert-success');
        } else {
            alert.innerHTML = '<b>Sorry, you are wrong! The right answer is</b> - "' +  answer['content'] + '"';
            alert.classList.add('alert-danger');
        }
        let answers = document.getElementsByName('question-id-' + answer['question_id']);

        for (let i = 0, l = answers.length; i < l; ++i) {
            answers[i].disabled = true;
        }
    }
</script>
