<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
                .alert {
                        min-height: 50px;
                    }
        </style>
        <title>flat rock tech</title>
    </head>
    <body>
        <!-- Image and text -->
        <nav class="navbar navbar-light bg-light">
            <a class="navbar-brand" href="/">Flat Rock Tech</a>
        </nav>
        <div class="container mt-1">
            <div class="card">
                <div class="card-header d-flex">
                    <h3 class="bd-highlight">English Test</h3>
                    <div class="ml-auto">
                        <a href="javascript:void(0)"
                           class='btn btn-warning'
                           onclick="resetTest()"
                        >
                            Reset
                        </a>
                        <a href="/"
                           class='btn btn-primary
                           <? if ($type == 'multiple') { echo 'disabled'; } ?>'
                        >
                            Multiple
                        </a>
                        <a href="/binary"
                           class="btn btn-primary <? if ($type =='binary') { echo 'disabled'; }?>"
                        >
                            Binary
                        </a>
                    </div>
                </div>
                <div class="form p-2">
                    <?php
                    if($type === 'multiple'){
                        include_once ('multipleTest.php');
                    } elseif ($type === 'binary') {
                        include_once ('binaryTest.php');
                    } else {
                        echo '<h2>Oops something went wrong</h2>';
                    }
                    ?>
            </div>
        </div>
            <script>
                function resetTest() {
                    let alert = document.getElementsByClassName('alert')
                    for (let i = 0, l = alert.length; i < l; ++i) {
                        alert[i].classList.remove('alert-success');
                        alert[i].classList.remove('alert-danger');
                        alert[i].innerHTML = null;
                    }
                    let inputs = document.getElementsByTagName('input')
                    for (let i = 0, l = inputs.length; i < l; ++i) {
                        inputs[i].removeAttribute('disabled');
                        inputs[i].checked = false;
                    }
                }
            </script>
    </body>
</html>
