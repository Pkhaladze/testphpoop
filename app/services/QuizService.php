<?php

namespace app\services;

class QuizService
{
    public static function concatenateQuestionsWithAnswers($questions, $answers)
    {
        $questionsWithAnswers = [];

        //collect related answers and insert into questions array
        foreach ($questions as $question) {
            $answerKeys = array_keys(array_column($answers, 'question_id'), $question['id']);
            $selectedAnswers = [];

            foreach ($answerKeys as $answerKey) {
                $selectedAnswers[]=$answers[$answerKey];
            }

            $question['answers'] = $selectedAnswers;
            $questionsWithAnswers[] = $question;

        }

        return $questionsWithAnswers;
    }

    public static function getMultipleCorrectAnswer($answers, $answerId)
    {
        //initialised response content
        $response = [
            'status' => 'error',
            'content' => 'Something went wrong. The answer was not found',
            'correct' => false
        ];

        //choose correct answer
        $key = array_search('true', array_column($answers, 'correct'));

        if ($key === false){
            return $response;
        }

        //check chosen answer and return correct one
        $response['status'] = 'success';
        $response['content'] = $answers[$key]['answer'];
        $response['question_id'] = $answers[$key]['question_id'];
        if ($answers[$key]['id'] == $answerId){
            $response['correct'] = true;
        }

        return $response;
    }

    public static function getBinaryCorrectAnswer($answer,  $answered){
        //initialised response content
        $response = [
            'status' => 'error',
            'content' => 'Something went wrong, The answer was not found',
            'correct' => false
        ];

        if (empty($answer)){
            return $response;
        }

        $response['status'] = 'success';
        $response['content'] = $answer['type'];
        $response['id'] = $answer['id'];

        //check chosen answer and return correct one
        if ($answer['type'] == $answered){
            $response['correct'] = true;
        }

        return $response;
    }
}
