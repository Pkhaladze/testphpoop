
<?php
use app\core\Request;
use app\core\Router;
use app\controllers\HomeController;


$router = new Router(new Request);


/**
 * Add new route here;
 */

$router->get('/', function() {

    return HomeController::home();

});


$router->get('/binary', function() {

    return HomeController::binaryQuestions();

});


$router->post('/multiple-answer', function($request) {

     return HomeController::multipleAnswers($request->getBody());

});

$router->post('/binary-answer', function($request) {

     return HomeController::binaryAnswers($request->getBody());

});
