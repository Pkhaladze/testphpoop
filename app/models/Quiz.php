<?php


namespace app\models;
use app\services\QuizService;

class Quiz extends MainModel
{
    public function getAllMultipleQuestionsWithAnswers() {

        $queryQuestions = "SELECT * FROM questions WHERE type = " . "'multiple'" . " ORDER BY RAND() LIMIT 10";
        $questions = $this->get($queryQuestions);

        $queryAnswers = 'SELECT * FROM answers';
        $answers = $this->get($queryAnswers);

        $questionsWithAnswers = QuizService::concatenateQuestionsWithAnswers($questions, $answers);

        return $questionsWithAnswers;

    }

    public function getAllBinaryQuestionsWithAnswers() {

        $queryQuestions = "SELECT * FROM questions WHERE type <> " . "'multiple'";
        $questions = $this->get($queryQuestions);

        return $questions;

    }

    public function getMultipleAnswers($request) {

        $queryQuestions = "SELECT * FROM answers WHERE question_id = $request[questionId]";
        $answers = $this->get($queryQuestions);
        $answer = QuizService::getMultipleCorrectAnswer($answers, $request['answerId']);
        return $answer;

    }

    public function getBinaryAnswers($request){

        $queryQuestions = "SELECT * FROM questions WHERE id = $request[questionId]";
        $getAnswer = $this->get($queryQuestions);
        $answer = QuizService::getBinaryCorrectAnswer($getAnswer[0], $request['answer']);
        return $answer;
    }
}
