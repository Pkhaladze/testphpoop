<?php

namespace app\core;

interface RequestInterface
{
    public function getBody();
}
