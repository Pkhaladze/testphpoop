<?php

namespace app\DB;
use PDO;

class MySql {

    private function _con() {

        $pdo = new PDO(
            'mysql:host=' . env('DB_HOST') . ';port=' . env('DB_PORT') . ';dbname=' . env('DB_NAME') . ';charset=utf8',
            env('DB_USERNAME'),
            env('DB_PASSWORD')
        );
        $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }

    public function query($query, $params = array()) {
        $stmt = $this->_con()->prepare($query);
        $stmt->execute($params);
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

}
