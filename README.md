# TestPHPOOP

## MySQL Dump
Use this dump script below to migrate tables and seed data into the database.
https://gitlab.com/Pkhaladze/testphpoop/snippets/1883453 

### ENV
Copy `env.example.php` to `env.php` and change credentials with yours


### Run local server
Run this command in the root directory `php -S localhost:1234`

### Requirements
`PHP >= 7.1` 

`MySql = 5.7`
