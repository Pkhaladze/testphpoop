
<?php

spl_autoload_register('AutoLoader');
function AutoLoader($className)
{

    // imports files based on the namespace as folder and class as filename.
    $file = str_replace('\\',DIRECTORY_SEPARATOR, $className);
    require_once './app/core/Env.php';
    require_once $file . '.php';
}
